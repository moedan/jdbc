import java.sql.*;
import oracle.jdbc.*;
import oracle.jdbc.pool.OracleDataSource;
public class dbconnection {
	public static void main (String args[])
			 throws SQLException
			 {
			 OracleDataSource ods = new OracleDataSource();
			 ods.setURL("jdbc:oracle:thin:scott/tiger@host:port/service");
			 Connection conn = ods.getConnection();
			 // Create Oracle DatabaseMetaData object
			 DatabaseMetaData meta = conn.getMetaData();
			 // gets driver info:
			 System.out.println("JDBC driver version is " + meta.getDriverVersion());
			 }
}
